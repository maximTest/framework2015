import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.VideoRecorder;

import java.lang.reflect.Method;

import static org.testng.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 2/18/15
 * Time: 4:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestOpenNews extends TestBase {
    MainPage mainPage;
    private VideoRecorder recordVideo;


    @BeforeClass
   public void testOpenNews(ITestContext iTestContext){
         webDriver = (WebDriver) iTestContext.getAttribute(iTestContext.getCurrentXmlTest().getName());
         mainPage = new MainPage(webDriver);
   }

   @BeforeMethod(alwaysRun = true)
   public void initRecord(ITestContext iTestContext){
        recordVideo = new VideoRecorder();
        recordVideo.startRecording(webDriver,iTestContext.getOutputDirectory());

   }

    @Test
    public void checkTextField(Method method){
        try{
        assertTrue(" Test one".equals(mainPage.getTextTitleWithWhiteSpace())," Actual result:"
                + mainPage.getTextTitleWithWhiteSpace() + " doesn't equal Expected result:" + " Test one\n");
        }finally {
            recordVideo.stopRecording(method.getName());
        }
    }

    @Test
    public void checkAnotherTextField(Method method){
        try{
        assertTrue("Test one".equals(mainPage.getTextTitleWithWhiteSpace())," Actual result:"
                + mainPage.getTextTitleWithWhiteSpace() + " doesn't equal Expected result:" + " Test one\n");
        } finally {
            recordVideo.stopRecording(method.getName());
        }
    }
}
