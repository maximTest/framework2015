import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.Test;
import properties.PropertyLoader;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 4/10/15
 * Time: 6:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginPage extends TestBase {

    @Test
    public void openUrl(ITestContext iTestContext){
        String websiteUrl = PropertyLoader.loadProperties("site.url");
        webDriver = (WebDriver) iTestContext.getAttribute(iTestContext.getCurrentXmlTest().getName());
        webDriver.navigate().to(websiteUrl);
    }

}
