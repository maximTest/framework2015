package listener;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 4/9/15
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class ListenerHighlightsElementsBeforeActions implements WebDriverEventListener {

    private String color ;
    private String bgColor;

    public ListenerHighlightsElementsBeforeActions() {

    }

    public ListenerHighlightsElementsBeforeActions(String color) {
        this.color = color;
    }

    private void setBgColor(WebElement element) {
        bgColor  = element.getCssValue("backgroundColor");
    }


    private void changeColor(WebElement element, WebDriver driver ,String color) {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("arguments[0].style.backgroundColor ='"+color+"'",  element);
    }





    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver webDriver) {
        setBgColor(webDriver.findElement(by));
        changeColor(webDriver.findElement(by),webDriver,color);

    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver webDriver) {
        changeColor(webDriver.findElement(by),webDriver,bgColor);
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver webDriver) {
        setBgColor(element);
        changeColor(element,webDriver,color);
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver webDriver) {
        changeColor(element,webDriver,bgColor);
    }

    @Override
    public void beforeChangeValueOf(WebElement element, WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterChangeValueOf(WebElement element, WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
