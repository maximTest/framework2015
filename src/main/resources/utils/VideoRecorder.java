package utils;

import org.monte.media.Format;
import org.monte.media.FormatKeys.*;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.*;



public class VideoRecorder {

    private ScreenRecorder screenRecorder;
    private File dir;

    public void startRecording(WebDriver webDriver,String path) {

        try {
            GraphicsConfiguration gc = GraphicsEnvironment
                    .getLocalGraphicsEnvironment().getDefaultScreenDevice()
                    .getDefaultConfiguration();

            dir = new File(path);

            Point point = webDriver.manage().window().getPosition();
            Dimension dimension = webDriver.manage().window().getSize();

            Rectangle rectangle = new Rectangle(point.x, point.y,
                    dimension.width, dimension.height);

            screenRecorder = new ScreenRecorder(gc, rectangle,
                    new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey,
                            MIME_AVI),
                    new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey,
                            ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                            CompressorNameKey,
                            ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, DepthKey,
                            24, FrameRateKey, Rational.valueOf(15), QualityKey,
                            1.0f, KeyFrameIntervalKey, 15 * 60), new Format(
                    MediaTypeKey, MediaType.VIDEO, EncodingKey,
                    "black", FrameRateKey, Rational.valueOf(30)), null,
                    dir);

            screenRecorder.start();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void stopRecording(String recordName) {

        try {
            screenRecorder.stop();
            if (recordName != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd HH.mm.ss");
                File newFileName = new File(String.format("%s%s %s.avi",
                          dir,
                         recordName,
                        dateFormat.format(new Date())));

                screenRecorder.getCreatedMovieFiles().get(0)
                        .renameTo(newFileName);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
