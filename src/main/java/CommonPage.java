import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.GlobalVariable;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 2/18/15
 * Time: 3:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommonPage {
    protected WebDriver webDriver;
    public CommonPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    /**
     * Method wait for all Ajax Calls end or for GlobalTimeOut seconds
     */
    public void waitForEndOfAllAjaxes(){
        WebDriverWait wait = new WebDriverWait(webDriver, GlobalVariable.GlobalTimeOut);
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(org.openqa.selenium.WebDriver driver) {
                return (Boolean)((JavascriptExecutor)driver).executeScript("return jQuery.active == 0");
            }
        });
    }
}
