import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 2/18/15
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainPage extends CommonPage {

    @FindBy(css ="a[href='/categories']")
     private WebElement buttonNews;

    @FindBy(css ="a[href='/categories']")
    private List <WebElement> buttonNews2;

    @FindBy(xpath = "//span/b")
     private  WebElement innerTextTag;


    MainPage(WebDriver webDriver){
        super(webDriver);
       // waitForEndOfAllAjaxes();

    }

    MainPage openNews(){
        new Actions(webDriver).dragAndDrop(buttonNews,innerTextTag).perform();
        buttonNews.click();
        return new MainPage(webDriver);
    }


  // Get text from textNode with whitespace
  // getText() in SeleniumWebDriver get the innerText without any leading or trailing whitespace.
    public String getTextTitleWithWhiteSpace(){
          return innerTextTag.getAttribute("innerHTML");
    }

}
